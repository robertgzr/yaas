FROM golang:1.11-alpine as build

RUN apk add -U curl git make
RUN curl -sSL -o /bin/youtube-dl https://github.com/rg3/youtube-dl/releases/download/2019.02.08/youtube-dl
RUN chmod +x /bin/youtube-dl

COPY . /src/yaas
WORKDIR /src/yaas

ENV GOBIN=/bin
RUN make

FROM python:3-alpine

COPY --from=build \
    /bin/youtube-dl \
    /usr/bin/youtube-dl

COPY --from=build \
    /bin/yaas \
    /usr/bin/yaas

EXPOSE 3796
ENTRYPOINT ["yaas", "--serve"]
