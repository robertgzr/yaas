GOBIN ?= ./bin
IMAGE ?= robertgzr/yaas

all: build

build: clean
	CGO_ENABLED=0 go build -ldflags='-s' -o $(GOBIN)/yaas ./cmd/yaas

clean:
	rm -rf $(GOBIN)/yaas

dbuild:
	docker build -t $(IMAGE) -f Dockerfile .

.PHONY: build dbuild clean
