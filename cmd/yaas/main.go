package main

import (
	"context"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"syscall"

	flag "github.com/spf13/pflag"

	"github.com/apex/log"
	"github.com/julienschmidt/httprouter"
	"github.com/oklog/run"
	"github.com/pkg/errors"
	"github.com/robertgzr/rlog"

	"moji.moe/go/yaas"
	"moji.moe/go/yaas/handlers"
)

var (
	url   string
	bin   string
	serve bool
	debug bool
)

func main() {
	flag.StringVar(&url, "url", "", "url to resolve")
	flag.StringVar(&bin, "bin", "", "path to the youtube-dl binary")
	flag.BoolVarP(&serve, "serve", "s", false, "start the http service")
	flag.BoolVar(&debug, "debug", false, "enable verbose logging")
	flag.Parse()

	log.SetHandler(rlog.Default)
	if debug {
		log.SetLevel(log.DebugLevel)
	}

	var err error
	if bin == "" {
		bin, err = exec.LookPath("youtube-dl")
		if err != nil {
			log.Fatal(err.Error())
		}
	}

	var (
		ctx = context.Background()
		s   = yaas.NewService(bin)
	)

	if !serve {
		runOnce(s, url)
	}

	r := &httprouter.Router{
		RedirectTrailingSlash:  false,
		RedirectFixedPath:      false,
		HandleMethodNotAllowed: false,
		HandleOPTIONS:          false,
	}
	r.Handler("GET", handlers.StatsEndpoint, handlers.Stats(ctx))
	r.Handler("GET", handlers.GetURLEndpoint+"*url", handlers.GetURL(ctx, s))

	var g run.Group
	{
		var (
			addr = ":3796"
		)
		ln, err := net.Listen("tcp", addr)
		if err != nil {
			log.Fatal(err.Error())
		}
		g.Add(func() error {
			log.Infof("Listening on %s", addr)
			return http.Serve(ln, r)

		}, func(err error) {
			ln.Close()
			log.Warnf("Listening stopped: %s", err)
		})
	}
	{
		sigs := make(chan os.Signal, 1)
		done := make(chan struct{}, 1)
		g.Add(func() error {
			signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
			select {
			case sig := <-sigs:
				return errors.Errorf("Signal received: %v", sig)
			case <-done:
				return nil
			}

		}, func(err error) {
			log.Debugf("Signal handler was interrupted: %s", err)
			done <- struct{}{}
		})
	}
	if err := g.Run(); err != nil {
		log.Warnf("Aborted: %s", err)
	}
}

func runOnce(s yaas.Service, url string) {
	video, err := s.GetVideoURL(context.Background(), url)
	if err != nil {
		log.Fatal(err.Error())
	}
	println(video)
	os.Exit(0)
}
