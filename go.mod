module moji.moe/go/yaas

require (
	github.com/VividCortex/gohistogram v1.0.0 // indirect
	github.com/apex/log v1.1.0
	github.com/fatih/color v1.7.0 // indirect
	github.com/go-kit/kit v0.8.0
	github.com/julienschmidt/httprouter v1.2.0
	github.com/mattn/go-colorable v0.1.0 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/oklog/run v1.0.0
	github.com/pkg/errors v0.8.1
	github.com/robertgzr/rlog v0.0.0-20180417044125-7651948cc8b8
	github.com/spf13/pflag v1.0.3
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/sys v0.0.0-20190213121743-983097b1a8a3 // indirect
)
