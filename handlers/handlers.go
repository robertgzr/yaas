package handlers

import (
	"context"
	"net/http"
	"strings"
	"time"

	"github.com/apex/log"
	"github.com/go-kit/kit/metrics/generic"

	"moji.moe/go/yaas"
)

const GetURLEndpoint = "/geturl/"

func GetURL(ctx context.Context, s yaas.Service) http.Handler {
	metrics.latency[GetURLEndpoint] = generic.NewHistogram(GetURLEndpoint, 50)

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		begin := time.Now()
		defer metrics.latency[GetURLEndpoint].Observe(time.Since(begin).Seconds())

		uri := strings.TrimPrefix(r.URL.String(), GetURLEndpoint)
		log.WithField("uri", uri).Debug("Request")

		videoURL, err := s.GetVideoURL(ctx, uri)
		if err != nil {
			handleError(w, r, err)
			return
		}
		log.WithField("uri", videoURL).Debug("Redirecting")
		log.Infof("Sucess, redirecting from %q", uri)
		http.Redirect(w, r, videoURL, http.StatusFound)
	})
}

func handleError(w http.ResponseWriter, r *http.Request, err error) {
	log.Errorf("Handle error: %s", err)
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}
