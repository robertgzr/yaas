package handlers

import (
	"bytes"
	"context"
	"net/http"
	"strings"
	"time"

	"github.com/go-kit/kit/metrics/generic"
)

var metrics metricStore

type metricStore struct {
	latency map[string]*generic.Histogram
}

func init() {
	metrics = metricStore{
		latency: make(map[string]*generic.Histogram),
	}
}

var StatsEndpoint = "/stats"

func Stats(ctx context.Context) http.Handler {
	metrics.latency[StatsEndpoint] = generic.NewHistogram(StatsEndpoint, 50)

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		begin := time.Now()
		defer metrics.latency[StatsEndpoint].Observe(time.Since(begin).Seconds())

		var resp = new(bytes.Buffer)
		resp.WriteString(":: site stats ::\n")
		resp.WriteString(time.Now().String() + "\n\n")
		for route, latencyHistogram := range metrics.latency {
			resp.WriteString(route + "\n")
			underline := strings.Map(func(_ rune) rune {
				return '='
			}, route)
			resp.WriteString(underline + "\n")
			latencyHistogram.Print(resp)
			resp.WriteString("\n")
		}

		w.WriteHeader(200)
		w.Write(resp.Bytes())
	})
}
