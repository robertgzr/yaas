package yaas

import (
	"context"
	"io/ioutil"
	"os/exec"
	"strings"

	"github.com/pkg/errors"
)

type Service interface {
	GetVideoURL(ctx context.Context, from string) (to string, err error)
}

type svc struct {
	bin string
}

func NewService(binpath string) Service {
	return &svc{
		bin: binpath,
	}
}

func (s *svc) GetVideoURL(ctx context.Context, from string) (to string, err error) {
	c := exec.Command(s.bin, "-g", "-f", "best", from)

	stderr, err := c.StderrPipe()
	if err != nil {
		return "", err
	}
	stdout, err := c.StdoutPipe()
	if err != nil {
		return "", err
	}
	if err := c.Start(); err != nil {
		return "", err
	}

	errbuf, err := ioutil.ReadAll(stderr)
	if err != nil {
		return "", err
	}
	outbuf, err := ioutil.ReadAll(stdout)
	if err != nil {
		return "", err
	}

	if err := c.Wait(); err != nil {
		return "", errors.New(string(errbuf))
	}
	return strings.TrimSpace(string(outbuf)), nil
}
